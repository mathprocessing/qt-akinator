#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "json.h"

#include <QFileDialog>
#include <QDebug>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QRadioButton>
#include <QVBoxLayout>
#include <QWidget>
#include <QButtonGroup>

#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>
#include <QMessageBox>
#include <QtMath>

QString showProbability(double p) {
  // QString::number(double n, char format = 'g', int precision = 6)
  return QString::number(p * 100.0, 'g', 3);
}

QString showVector(const QVector<double>& v) {
  QString s = "[";
  for (int i = 0; i < v.size(); i++) {
    if (i > 0) s += ", ";
    s += QString::number(v[i]);
  }
  return s + "]";
}
QVector<double> jsonArrayToVector(const QJsonArray& jsonArray) {
  QVector<double> result;
  for (int i = 0; i < jsonArray.size(); i++) {
    result.push_back(jsonArray[i].toDouble());
  }
  return result;
}
QVector<QString> jsonArrayToVectorString(const QJsonArray& jsonArray) {
  QVector<QString> result;
  for (int i = 0; i < jsonArray.size(); i++) {
    result.push_back(jsonArray[i].toString());
  }
  return result;
}

unsigned argMax(const QVector<double>& v) {
  double maxValue = -1.0;
  unsigned maxIndex = 0;
  for (int i = 0; i < v.size(); i++) {
    if (v[i] > maxValue) {
      maxValue = v[i];
      maxIndex = i;
    }
  }
  return maxIndex;
}

QVector<double>& normalize(QVector<double>& v) {
  double total = 0.0;
  for (int i = 0; i < v.size(); i++) {
    Q_ASSERT(v[i] >= 0.0);
    total += v[i];
  }
  for (int i = 0; i < v.size(); i++) {
    if (total <= __DBL_EPSILON__) {
      v[i] = 1.0 / v.size();
    } else {
      v[i] /= total;
    }
  }
  return v;
}
QVector<double>& product(QVector<double>& v, QVector<double>& u) {
  Q_ASSERT(v.size() == u.size());
  for (int i = 0; i < v.size(); i++) {
    v[i] *= u[i];
  }
  return v;
}

double entropy(QVector<double>& v) {
  double total = 0.0;
  for (int i = 0; i < v.size(); i++) {
    if (v[i] > 0.0) {
      total += v[i] * qLn(v[i]);
    }
  }
  return -total  / M_LN2;
}

struct QuAnswer {
  unsigned questionId;
  unsigned answerId;
  QuAnswer(unsigned questionId_, unsigned answerId_)
    : questionId(questionId_)
    , answerId(answerId_)
  {}
};

struct Question {
  QString text;
  QVector<QVector<double>>* answers;
  static Question fromJsonObject(const QJsonObject& question) {
    Question q;
    q.text = question["text"].toString();
    q.answers = new (QVector<QVector<double>>);
    auto rows = question["answers"].toArray();
    for (int chId = 0; chId < rows.size(); chId++) {
      QVector<double> v = jsonArrayToVector(rows[chId].toArray());
      q.answers->push_back(v);
    }
    return q;
  }

  QString toString() {
    return this->text;
  }
};

QVector<double> quAnswerToProbDist(const QVector<Question>& questions, QuAnswer quAnswer) {
  Question currentQuestion = questions[quAnswer.questionId];
  auto rows = currentQuestion.answers;
  QVector<double> result;
  for (int characterId = 0; characterId < rows->size(); characterId++) {
    double prob = rows->at(characterId).at(quAnswer.answerId);
    result.push_back(prob);
  }
  return normalize(result);
}

enum GameState {
  NORMAL,
  GAMEOVER,
};

bool anyJsonError = false;

void showError(QJsonParseError& err) {
  QMessageBox::critical(nullptr, "Json error", err.errorString());
  anyJsonError = true;
}

/* {Tiger: 0.01, Giraffe: 0.98, Other: 0.01} */
QString showHypInfo(const QVector<double>& pd, const QVector<QString>& labels) {
  Q_ASSERT(pd.size() == labels.size());
  QString s = "{";
  for (int i = 0; i < pd.size(); i++) {
    if (i > 0) s += ", ";
    s += labels[i] + ": " + QString::number(pd[i]);
  }
  return s + "}";
}

class Game {
public:
  GameState state = NORMAL;

  Ui::MainWindow* ui;

  // Results
  int resultCharacterId = -1; // -1 = no max character to be saved

  unsigned numberOfAnswers;
  unsigned numberOfCharacters;
  unsigned numberOfAnsweredQuestions = 0;
  QVector<QString> answerLabels; // Yes, No ...
  QVector<QString> characterLabels; // Giraffee, Tiger, ...

  QVector<Question> questionList;

  QSet<unsigned> setOfAlreadyAskedQuestions; // insert, contains
  QSet<unsigned> setOfAllQuestions;
  QVector<QuAnswer> askedQAList;

  QVector<double> hypDistribution;
  QVector<int> characterCounts; // [1, 1, 1]

  unsigned currentQuestionId = 0;

  Game(Ui::MainWindow* ui) {
    QJsonParseError err;

    QJsonDocument charactersJson = loadJsonFromFile("characters.json", &err);
    if (err.error != QJsonParseError::NoError) showError(err);

    QJsonDocument questionsJson = loadJsonFromFile("questions.json", &err);
    if (err.error != QJsonParseError::NoError) showError(err);

    QJsonDocument answerLabelsJson = loadJsonFromFile("answers.json", &err);
    if (err.error != QJsonParseError::NoError) showError(err);

    if (anyJsonError) {
      exit(1);
    }

    this->numberOfAnswers = answerLabelsJson.array().size();
    this->numberOfCharacters = charactersJson.array().size();
    this->answerLabels = jsonArrayToVectorString(answerLabelsJson.array());

    // Init characters distribution

    for (unsigned i = 0; i < this->numberOfCharacters; i++) {
       double count = charactersJson.array()[i].toObject()["count"].toDouble();
       this->characterCounts.push_back(count);
       this->hypDistribution.push_back(count);
       QString chName = charactersJson.array()[i].toObject()["name"].toString();
       this->characterLabels.push_back(chName);
    }
    normalize(this->hypDistribution);
    // -------------------------------

    QJsonArray questionsArray = questionsJson.array();

    for (int i = 0; i < questionsArray.size(); i++) {
      QJsonObject question = questionsArray[i].toObject();
      this->questionList.push_back(Question::fromJsonObject(question));
      this->setOfAllQuestions.insert(i);
    }

    this->ui = ui;
    this->start();
  }

  unsigned getRandomQuestionId() {
    QSet<unsigned> otherQs = setOfAllQuestions.subtract(setOfAlreadyAskedQuestions);
    QList<unsigned> list = otherQs.values();
    if (list.size() > 0) {
      setOfAlreadyAskedQuestions.insert(list[0]);
      return list[0];
    } else {
      return -1; // no questions
    }
  }

  void saveFiles() {
    qDebug() << "Save files";
    // Save character counts
    // Change +1 to win character
    if (resultCharacterId == -1) {
      return;
    }

    characterCounts[resultCharacterId]++;

    QJsonArray chs;
    for (unsigned i = 0; i < numberOfCharacters; i++) {
      QJsonObject chObject;
      chObject.insert("name", QJsonValue(characterLabels[i]));
      chObject.insert("count", QJsonValue(characterCounts[i]));
      chs.append(chObject);
    }

    saveJsonToFile(QJsonDocument(chs), "characters.json");
    // Save questions and number of answers for each character and question

//    currentQuestionId, answerId
    // 1. Modify answers matrix
    for (int i = 0; i < askedQAList.size(); ++i) {
      unsigned questionId = askedQAList[i].questionId;
      unsigned answerId = askedQAList[i].answerId;
//      unsigned value = questionList[questionId].answers->at(resultCharacterId)[answerId];
      (*questionList[questionId].answers)[resultCharacterId][answerId]++;
    }


    // 2. Save
    QJsonArray qs;
    for (int i = 0; i < questionList.size(); i++) {
      Question q = questionList[i];
      QJsonObject question;
      question.insert("text", q.text);
      auto matrix2dPointer = q.answers;
      QJsonArray matrix2d;
      for (unsigned j = 0; j < numberOfCharacters; j++) {
        QJsonArray row;
        for (unsigned k = 0; k < numberOfAnswers; k++) {
          int oldValue = (*matrix2dPointer)[j][k];
          row.append(oldValue);
        }
        matrix2d.append(row);
      }
      question.insert("answers", matrix2d);
      qs.append(question);
    }
    saveJsonToFile(QJsonDocument(qs), "characters_new.json");
  }

  void showResults() {
    unsigned maxIndex = argMax(hypDistribution);
    resultCharacterId = maxIndex;
    ui->caption1->setText("Результаты:");
    ui->question_lbl->setText(
          "Предполагаю что Вы загадали " + characterLabels[maxIndex] +
          " с вероятностью " + showProbability(hypDistribution[maxIndex]) + "%. "
          "Можете нажать 'сохранить оценку' если считаете что акинатор угадал Вашего персонажа.");
    ui->saveButton->setEnabled(true);
  }

  void start() {
    ui->saveButton->setEnabled(false);
    this->nextQuestion();
  }

  void restart() {
    qDebug("Restart game");
  }

  void setQuestionLabel(unsigned questionId) {
    Question question = questionList[questionId];
    ui->question_lbl->setText(question.text);
  }

  void nextQuestion() {
    if (this->state == GAMEOVER) {
        qDebug() << "GAMEOVER";
        return;
    }
    ui->caption1->setText("Вопрос №" + QString::number(this->numberOfAnsweredQuestions + 1) + ":");
    ui->info->setText(showHypInfo(this->hypDistribution, characterLabels));
    int questionId = getRandomQuestionId();
    if (questionId == -1) {
      this->state = GAMEOVER;
      this->showResults();
      return;
    }
    currentQuestionId = questionId;

    setQuestionLabel(currentQuestionId);

  }

  void addAnswer(unsigned answerId) {
    QuAnswer quAnswer = QuAnswer(currentQuestionId, answerId);
    askedQAList.push_back(quAnswer);

    // Change hyp distribution
    QVector<double> pd = quAnswerToProbDist(this->questionList, quAnswer);

    product(this->hypDistribution, pd);
    normalize(this->hypDistribution);

    numberOfAnsweredQuestions++;
  }

};

Game* game;
QVector<QRadioButton *> rButtons;
QVector<int> *test;
QButtonGroup group;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    game = new Game(ui);

    QVBoxLayout *box = new QVBoxLayout();

    for (unsigned i = 0; i < game->numberOfAnswers; i++) {
      rButtons.push_back(new QRadioButton(game->answerLabels[i]));
      group.addButton(rButtons[i]);
      box->addWidget(rButtons[i]);
    }

//    box->addWidget(bGroup);

    ui->g1->setLayout(box);

//    ui->chTable->clear();
//    ui->chTable->setRowCount(3);
//    for (auto r=0; r < 3; r++)
//      for (auto c=0; c < 2; c++)
//        ui->chTable->setItem( r, c, new QTableWidgetItem("Hello"));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_restartGame_clicked()
{
  game->restart();
}

void MainWindow::on_saveButton_clicked()
{
  game->saveFiles();
  ui->saveButton->setEnabled(false);
}

void MainWindow::on_nextQuestionButton_clicked()
{
   // Yes = -2, No = -3, ...
  int answerId = -(group.checkedId() + 2);
  qDebug() << "answerId = " << answerId;
  if (answerId == -1) {
    QMessageBox::information(this, "Info", "Если есть невыбранные ответы нельзя перейти к следующему вопросу");
    return;
  }
  Q_ASSERT(answerId >= 0);
  game->addAnswer(answerId);
  game->nextQuestion();
}

void MainWindow::on_action_exit_triggered()
{
   // Exit from Qt application
   exit(0);
}
