#include <QJsonDocument>
#include <QJsonObject>
#include <QDir>
#include <QFile>
#include <QDebug>
#include <QMessageBox>

QJsonDocument loadJsonFromFile(QString fileName, QJsonParseError *error = nullptr) {
//    qDebug() << QDir::currentPath();
//    QMessageBox::information(nullptr, "CurrentPath", QDir::currentPath());
//    QDir::setCurrent(pathToQuestions);


    QFile jsonFile(fileName);
    if (!jsonFile.open(QIODevice::ReadOnly))
    {
        QMessageBox::critical(nullptr, "Файл не найден", "Файл не найден в текущей директории: " + fileName);
        exit(1);
    }
    // Read the entire file
     QByteArray saveData = jsonFile.readAll();
     // Create QJsonDocument
     QJsonDocument jsonDocument(QJsonDocument::fromJson(saveData, error));
     if (error != nullptr && error->error != QJsonParseError::NoError) {
       qDebug() << error->errorString() << " in file: " << fileName;
     }
     return jsonDocument;
}

void saveJsonToFile(QJsonDocument jsonDocument, QString fileName) {
//    QDir::setCurrent(pathToQuestions);
    QFile jsonFile(fileName);
    if (!jsonFile.open(QIODevice::WriteOnly))
    {
        return;
    }
    // Write the current Json object to a file.
    jsonFile.write(jsonDocument.toJson(QJsonDocument::Indented));
    jsonFile.close();   // Close file
}
