#ifndef JSON_H
#define JSON_H
#include <QString>
#include <QJsonDocument>

QJsonDocument loadJsonFromFile(QString fileName, QJsonParseError *error = nullptr);
void saveJsonToFile(QJsonDocument jsonDocument, QString fileName);

#endif // JSON_H
